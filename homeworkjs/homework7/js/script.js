const myArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let mappedArray = myArray.map(function (el){
    return `<li>${el}</li>`;
});
let a = mappedArray.join(" ");

let ul = document.createElement('ul');
document.body.appendChild(ul);
ul.innerHTML += a;

let delay = 10;
let counter = document.getElementById('counter');
let timer = setInterval(function() {
    counter.innerHTML = --delay;
    if (!delay) {
        clearInterval(timer);
        document.querySelector("body").style.display='none';
    }
}, 1000);
counter.innerHTML = delay;
