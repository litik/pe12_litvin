let createNewUser = () => {
    let newUser = {};

    newUser.firstName = prompt('Please Enter first name');
    newUser.lastName = prompt('Please Enter last name');
    newUser.birthday = prompt('Please Enter Birthday dd.mm.yyyy:');

    newUser.getLogin = function(){
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };

    newUser.getAge = function ()
    {
        let today = new Date();
        let birthDate = new Date(this.birthday.substr(6,4)
            +'-'+this.birthday.substr(3,2)
            +'-'+this.birthday.substr(0,2));
        return  today.getFullYear() - birthDate.getFullYear();
    };

    newUser.getPassword = function(){
        return this.firstName.charAt(0).toLocaleUpperCase()
            + this.lastName.toLowerCase()
            + this.birthday.substr(6,4);
    };
    return newUser;
};

let newUser;

newUser = createNewUser();
console.log('newUser.firstName = "' + newUser.firstName + '"');
console.log('newUser.lastName = "' + newUser.lastName + '"');
console.log('newUser.getLogin = "' + newUser.getLogin() + '"');
console.log('newUser.getAge() = "' + newUser.getAge() + '"');
console.log('newUser.getPassword() = "' + newUser.getPassword() + '"');