/**
 * @desc Функция которая считает сумму двух чисел
 * @param {Number} a
 * @param {Number} b
 * @return {Number}
 **/
function sum(a, b) {
    return a + b;
}

/**
 * @desc Умножение двух чисел
 * @param {Number} a
 * @param {Number} b
 * @return {Number}
 **/
function multiply(a, b) {
    if (isFinite(a) && isFinite(b)) {
        return a * b;
    }

    return 0;
}

/**
 * @desc Подсчитывает кол-во вхождений числа в строку
 * @param {String} str
 * @param {Number} num
 * @return {Number}
 **/
function numInStr(str, num) {
    let counter = 0;

    if (!str) {
        return 0;
    }

    for (let i = 0; i < str.length; i++) {
        let s = str.charAt(i);

        if (s !== ' ' && +s === num) {
            counter++;
        }
    }

    return counter;
}

/**
 * @desc
 **/
function diff(a, b) {
    return a - b;
}

/**
 * @desc
 **/
function hockey(amountOfHits, goals) {
    if (!isFinite(amountOfHits) || !isFinite(goals)) {
        return 0;
    }

    return goals / amountOfHits * 100;

}