/**
 * @desc
 **/
function calc(callback, num) {
    let result = 0;

    if (typeof callback === 'function') {
        result = callback(num);
    }

    return result + num;

}