/**
 * @desc Create object
 * @return {Object}
 **/
function createNewUser(name, surname) {
    let newUser = {};

    Object.defineProperty(newUser, 'firstName', {
        enumerable: true,
        configurable: true,
        value: name
    });

    Object.defineProperty(newUser, 'lastName', {
        enumerable: true,
        configurable: true,
        value: surname
    });

    Object.defineProperty(newUser, 'getLogin', {
        enumerable: true,
        value: function () {
            return `${
                this.firstName
                    .charAt(0)
                    .toLowerCase()}${
                this.lastName
                    .toLowerCase()
                }`;
        }
    });

    return newUser;
}