(function () {
    document.addEventListener('DOMContentLoaded', onReady);

    window.onkeyup = function(e) {
        console.log(e.key, String.fromCharCode(e.keyCode));
    };

    let timer = null;
    let counter = 0;


    function showMsg(msgText, time, repeatance) {
            let id = setTimeout(function () {
                console.log(msgText);
               let idInterval = setInterval(function () {
                    counter++;
                    if (counter > repeatance) {
                        clearTimeout(id);
                        clearInterval(idInterval);
                    }
                   console.log(msgText);
                }, time);
            }, time);

    }

    function onReady() {
        const span = document.getElementById('timer');
        span.innerHTML = 0;

        timer = setInterval(function () {
            span.innerHTML = +span.innerHTML + 1;
        }, 1000);

        showMsg('God Bless you, Aleg', 4000, 4);
    }

    function onButtonClick() {
        if (timer) {
            clearInterval(timer);
        }
    }
})();

