document.addEventListener('DOMContentLoaded', onReady);

const hotKeys = [];

/**
 *
 **/
function onReady() {
    const txtAreas = document.getElementsByTagName('textarea');

    for(let txt of txtAreas) {
        txt.addEventListener('keypress', onTxtAreaKeyPress);
    }

    shortKey('shift', 's', function() {
        console.log('Нажата кнопка CTRL + S');
    });

//    shortKey('shift', 'k', function() {
//        console.log('Нажата кнопка CTRL + k');
//    });
//
//    shortKey('shift', 'j', function() {
//        console.log('Нажата кнопка CTRL + j');
//    });
}


function onCheckRepeat(e) {
    console.log(String.fromCharCode(e.keyCode || e.charCode), e.repeat);
}

/**
 *
 **/
function onTxtAreaKeyPress(e) {
    console.log(String.fromCharCode(e.keyCode || e.charCode), e.repeat);

    if (e.ctrlKey || e.altKey || e.metaKey || e.shiftKey) {

        hotKeys.forEach(function(item) {

            if (item.key2 === String.fromCharCode(e.keyCode || e.charCode).toLowerCase()) {
                if (typeof item.callback === 'function') {
                    item.callback();
                }
            }

        });
    }
}

/**
 * @desc
 **/
function shortKey(key1, key2, callback) {
    hotKeys.push({
        key1,
        key2,
        callback
    });
}