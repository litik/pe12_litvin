function onPressMeClick(e) {
    console.log('Browser event ->', e);

}

/**
 *
 **/
function onButton2Click(e) {
   console.log('Button 2 was clicked!!!', e);
}

function changeColorOfTheElement() {
    console.log('Change color...');
}

function changeSizeOfTheElement() {
    console.log('Change size of the element');
}

function changePositionOfTheElement() {
    console.log('Change position of the element');
}



let btn = document.getElementById('btnRegister');
let btnNext = document.getElementById('btnNextApproach');
let btnRemove = document.getElementById('btnRemoveEvent');

btn.onclick = onButton2Click;

btnRemove.onclick = function() {
    btnNext.removeEventListener('click', changeColorOfTheElement);
    console.log('------------');
};

btnNext.addEventListener('click', changeColorOfTheElement);

btnNext.addEventListener('click', changeSizeOfTheElement);

btnNext.addEventListener('click', changePositionOfTheElement);
