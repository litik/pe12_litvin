document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    const tables = document.getElementsByClassName('buggga');

    if (tables.length > 0) {
        tables[0].addEventListener('click', function (e) {
            const val = e.target.innerHTML;

            let div = document.getElementById('div1');

            if (!div) {
                div = document.createElement('div');
                div.id = 'div1';
                document.body.appendChild(div);
            }

            if (Number.isNaN(Number(div.innerHTML))) {
                div.innerHTML = 0;
            }

            div.innerHTML = Number(div.innerHTML) + Number(val);

        });
    }
}