(function () {
    const groupList = ['Andrienko Fedia', 'Bandura Andrew', 'bernatskyi.mykola', 'Dzhulay Vitya', 'Honcharuk Taras', 'Klymenko Lesia', 'Litvin Alexander', 'Makoviichuk Igor', 'Osipenko Nick', 'Sergey Donchenko', 'Shcherbyna Ruslan', 'Starodubets Misha', 'Taras Bashuk', 'Tkachenko Igor', 'Vladyslav', 'Voitiuk Liudmyla', 'Voytikh Kristina', 'Yuriy Sudarkin', 'alexeyku', 'AnastasiaG', 'Anna', 'Biliavska Tetiana', 'Britan Oleg', 'Daniel', 'Daria', 'Dovgusha Vladislav', 'Dubinsky Dmitriy', 'Elithabeth', 'Ivanov Ihor', 'Kateryna', 'Kotyk_Roman', 'Kseniia', 'Logvinenko Anton', 'Lozovyi Volodymyr', 'Moroziuk Mariia', 'Mykhailo Hordiienko', 'Oleksandr Kalamurza', 'Ovcherenko Anton', 'Rodya', 'Rublenko Vladyslav', 'Skrypka Oleg', 'Ustinov Yehor', 'Vel Yura', 'XavierRuf', 'yamnyk', 'zozich', 'Брана Наталія', 'Шулыгин Марк'];

    document.addEventListener('DOMContentLoaded', onReady);

    /***
     * @descr onReady
     */
    function onReady() {
        const btn = document.getElementById('randVctm');


        btn.onclick = function () {
           let counter= 0;

            do {
                if (counter >groupList.length) {
                    break;
                }
                const rand = Math.round(Math.random() * groupList.length);
                const date = new Date();
                let strDate = `${date.getDay()}${date.getMonth()}${date.getFullYear()}_${rand}`;
                counter++;
                if (localStorage.getItem(strDate)) {
                    continue;
                }

                let qst = confirm(`Есть ли в аудитории ${groupList[rand]} ?`);
                if (qst && !localStorage.getItem(strDate)) {
                    localStorage.setItem(strDate, groupList[rand]);
                    break;
                }
            } while (true);



        }
    }
})();

