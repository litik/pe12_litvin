/**
 * @desc
 * @return {String}
 **/
function firstLetterCapital(str) {
    return `${trim(str).charAt(0).toUpperCase()}${trim(str).slice(1)}`;
}

/**
 * @desc
 * @return {String}
 **/
function getSubString(str, start = 0, end = 0) {
    return trim(str).substr(start, end);
    // return str.substring(start, end);
    // return str.slice(start, end);
}

/**
 *
 **/
function trim(str) {
    return `${str.trim()}`;
}

/**
 * @return {String}
 **/
function charAt(str, index) {
    return str[index];
}

function getIndexByChar(str, char) {
    return str.indexOf(char);
}


