
document.addEventListener("click", function (event) {

    if (event.target.classList.contains("btn-filterSelection")) {
        let innerTxt = event.target.innerText.split(' ').join('-').toLowerCase();
        filterSelection(innerTxt);
    }

    if(event.target.classList.contains('tabs-title')){

        document.querySelector('.tabs-title.active').classList.remove('active');
        document.querySelector('.tabs-content.active').classList.remove('active');
        document.querySelector('.arrow.active').classList.remove('active');

        let theme = event.target.getAttribute('data-theme');
        document.querySelector(`.tabs-title[data-theme='${theme}']`).classList.add('active');
        document.querySelector(`.tabs-content[data-theme='${theme}']`).classList.add('active');
        document.querySelector(`.tabs-title[data-theme='${theme}']`).children[0].classList.add('active');
    }
});

function returnImgDiv(theme,num) {
    return `<div class="img ${theme}"><img class="img-item" src="img/${theme}/${theme}${num}.jpg" alt="NOT FOUND"><div class="hidden"><img class="img-hidden-left" src="img/img_hover1.png" alt="img NOT found"><img class="img-hidden-right" src="img/img_hover2.png" alt="img NOT found"><p class="blue img-text">CREATIVE DESING</p><p class="img-text2">${theme.split('-').join(" ")}</p></div></div>`
}

const imgContainer = document.querySelector('.img-container');
const graphicDesign = "graphic-design";
const webDesign = "web-design";
const landlingPage = "landing-page";
const wordPress = "wordpress";

filterSelection('all');

function imageAdding (count) {
    let numOfImgOnPage = (document.querySelectorAll('.img-item').length/4);
    for(let i=numOfImgOnPage+1; i<(count+numOfImgOnPage); i++) {
        imgContainer.innerHTML+=returnImgDiv(graphicDesign,i);
        imgContainer.innerHTML+=returnImgDiv(webDesign,i);
        imgContainer.innerHTML+=returnImgDiv(landlingPage,i);
        imgContainer.innerHTML+=returnImgDiv(wordPress,i);
    }
}

imageAdding(4);

function filterSelection(c) {
    let x, i;
    x = document.getElementsByClassName("img");
    if (c === "all") {
        c = "";
    }
    for (i = 0; i < x.length; i++) {
        removeClass(x[i], "show");
        if (x[i].className.indexOf(c) > -1) {
            addClass(x[i], "show");
        }
    }
}

function addClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) === -1) {
            element.className += " " + arr2[i];
        }
    }
}

function removeClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

filterSelection('all');

let btnContainer = document.getElementById("btn-container");
let btns = btnContainer.getElementsByClassName("btn-filterSelection");

for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function(){
        // filterSelection(`btns[i].innerHTML.substring(0,3)`);
        let current = document.getElementsByClassName("activeButton");
        current[0].className = current[0].className.replace(" activeButton", "");
        this.className += " activeButton";
    });
}

// загрузка
let loadButton = document.getElementById("loadButton");
loadButton.onclick = function () {
    let imgHidden = document.getElementsByClassName("img-hidden");
    document.getElementById("preloader").classList.remove("btn-hide");
    setTimeout(function () {
        imageAdding(4);
        filterSelection('all');
        document.getElementById("preloader").classList.add("btn-hide");
            // imgHidden[0].classList.remove("img-hidden");
            loadButton.classList.add("btn-hide");
    },2000);
};

// слайдер
let slideIndex = 1;
showSlides(slideIndex);

function plusSlide() {
    showSlides(slideIndex += 1);
}

function minusSlide() {
    showSlides(slideIndex -= 1);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("photo");
    let dots = document.getElementsByClassName("quotes-photo2");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace("activeSlideDot", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " activeSlideDot";
}
